﻿using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules
{
    public partial class CountyServiceLocations
    {
        public CountyServiceLocations()
        {
            CityServiceLocations = new HashSet<CityServiceLocations>();
        }

        public Guid Id { get; set; }
        public decimal? Tax { get; set; }
        public Guid? StateServiceId { get; set; }
        public int Created { get; set; }
        public bool? Active { get; set; }
        public int? Countyid { get; set; }

        public StateServiceLocations StateServiceLocation { get; set; }
        public ICollection<CityServiceLocations> CityServiceLocations { get; set; }
    }
}
