﻿/*
*CountryServiceLocation.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class CountyServiceLocation
    {
        public CountyServiceLocation(County County, StateServiceLocation StateServiceLocation, decimal? tax, bool active = true)
        {
            if (County == null)
                throw new ArgumentException("County cannot be null.");

            if (StateServiceLocation == null)
                throw new ArgumentException("StateServiceLocation cannot be null.");

            if (this.Tax != null && this.Tax < 0)
                throw new ArgumentException("Tax cannot be a negative decimal.");

            DateTimeOffset offset = new DateTimeOffset();
            this.Created = (int)offset.ToUnixTimeSeconds();
            this.County = County;
            this.CountyId = County.Id;
            this.StateServiceId = StateServiceLocation.Id;
            this.Tax = tax;
            this.StateServiceLocation = StateServiceLocation;
            this.Active = active;
            CityServiceLocations = new HashSet<CityServiceLocation>();
        }

        public Guid Id { get; set; }
        public Guid CountyId { get; set; }
        public Guid StateServiceId { get; set; }
        public decimal? Tax { get; set; }
        public int Created { get; set; }
        public bool Active { get; set; }

        public County County { get; set; }
        public StateServiceLocation StateServiceLocation { get; set; }
        public ICollection<CityServiceLocation> CityServiceLocations { get; set; }
    }
}
