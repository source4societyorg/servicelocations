﻿/*
*CountryServiceLocation.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class CountryServiceLocation
    {
        public CountryServiceLocation(Country Country, bool active = true)
        {
            if (Country == null)
                throw new ArgumentException("Country cannot be null");

            DateTimeOffset offset = new DateTimeOffset();         
            this.CountryId = Country.Id;
            this.Country = Country;
            this.Active = active;
            this.Created = (int)offset.ToUnixTimeSeconds();
            this.StateServiceLocations = new HashSet<StateServiceLocation>();
        }

        public Guid Id { get; set; }
        public int CountryId { get; set; }
        public int Created { get; set; }
        public bool Active { get; set; }

        public Country Country { get; set; }
        public ICollection<StateServiceLocation> StateServiceLocations { get; set; }
    }
}
