﻿/*
*ServiceLocation.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    class ServiceLocation
    {
        protected CountryServiceLocation CountryServiceLocation { get; set; }
        protected StateServiceLocation StateServiceLocation { get; set; }
        protected CountyServiceLocation CountyServiceLocation { get; set; }
        protected CityServiceLocation CityServiceLocation { get; set; }

        public ServiceLocation(
            CountryServiceLocation CountryServiceLocation = null
            ,StateServiceLocation StateServiceLocation = null
            ,CountyServiceLocation CountyServiceLocation = null
            ,CityServiceLocation CityServiceLocation = null
        )
        {
            this.CountryServiceLocation = CountryServiceLocation;
            this.StateServiceLocation = StateServiceLocation;
            this.CountyServiceLocation = CountyServiceLocation;
            this.CityServiceLocation = CityServiceLocation;
        }
    }
}
