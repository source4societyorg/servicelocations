﻿using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules
{
    public partial class Counties
    {
        public Guid Id { get; set; }
        public int Stateid { get; set; }
        public string Name { get; set; }
        public bool? Usercreated { get; set; }

        public State State { get; set; }
    }
}
