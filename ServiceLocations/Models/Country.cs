﻿/*
*Country.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class Country
    {
        public Country(string countryname, string countrycode, bool usercreated = true, bool active = true)
        {
            if (String.IsNullOrEmpty(countryname) || String.IsNullOrEmpty(countrycode))
                throw new ArgumentException("Country name and country code cannot be null or empty string");

            this.CountryName = countryname;
            this.CountryCode = countrycode;
            this.States = new HashSet<State>();
            this.CountryServiceLocations = new HashSet<CountryServiceLocation>();
            this.UserCreated = usercreated;
            this.Active = active;
        }

        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public bool Active { get; set; }
        public bool UserCreated { get; set; }

        public ICollection<State> States { get; set; }
        public ICollection<CountryServiceLocation> CountryServiceLocations { get; set; }
    }
}
