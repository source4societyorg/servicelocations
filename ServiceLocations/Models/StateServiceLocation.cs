﻿/*
*StateServiceLocation.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class StateServiceLocation
    {
        public StateServiceLocation(State State, CountryServiceLocation CountryServiceLocation, bool active = true)
        {
            if (State == null)
                throw new ArgumentException("State cannot be null");

            if (CountryServiceLocation == null)
                throw new ArgumentException("CountryServiceLocation cannot be null");

            DateTimeOffset offset = new DateTimeOffset();
            this.Created = (int)offset.ToUnixTimeSeconds();
            this.State = State;
            this.StateId = State.Id;
            this.CountryServiceLocation = CountryServiceLocation;
            this.Active = active;
            CountyServiceLocations = new HashSet<CountyServiceLocation>();
        }

        public Guid Id { get; set; }
        public int StateId { get; set; }
        public int Created { get; set; }
        public bool Active { get; set; }

        public State State { get; set; }
        public CountryServiceLocation CountryServiceLocation { get; set; }
        public ICollection<CountyServiceLocation> CountyServiceLocations { get; set; }
    }
}
