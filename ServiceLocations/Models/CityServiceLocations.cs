﻿using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules
{
    public partial class CityServiceLocations
    {
        public Guid Id { get; set; }
        public Guid CityId { get; set; }
        public Guid CountyServiceId { get; set; }
        public int Created { get; set; }
        public bool? Active { get; set; }

        public City City { get; set; }
        public CountyServiceLocations CountyServiceLocation { get; set; }
    }
}
