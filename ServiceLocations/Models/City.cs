﻿/*
*City.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class City
    {
        public City(Guid countyid, string cityname, bool usercreated = true, bool active = true)
        {
            if (String.IsNullOrEmpty(cityname))
                throw new ArgumentException("City name cannot be null or empty string");

            this.CountyId = countyid;
            this.CityName = cityname;
            this.UserCreated = usercreated;
            this.Active = active;
            this.CityServiceLocations = new HashSet<CityServiceLocation>(); 
        }

        public Guid Id { get; set; }
        public string CityName { get; set; }
        public Guid CountyId { get; set; }
        public bool Active { get; set; }
        public bool UserCreated { get; set; }

        public County County { get; set; }
        public ICollection<CityServiceLocation> CityServiceLocations { get; set; }
    }
}
