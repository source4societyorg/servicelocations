﻿using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules
{
    public partial class CountryServiceLocations
    {
        public CountryServiceLocations()
        {
            StateServiceLocations = new HashSet<StateServiceLocations>();
        }

        public Guid Id { get; set; }
        public int Created { get; set; }
        public bool? Active { get; set; }
        public int? Countryid { get; set; }

        public Country Country { get; set; }
        public ICollection<StateServiceLocations> StateServiceLocations { get; set; }
    }
}
