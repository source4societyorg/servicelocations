﻿/*
*State.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class State
    {
        public State(int countryid, string statename, string statecode, bool UserCreated = true, bool Active = true)
        {
            if (String.IsNullOrEmpty(statename) || String.IsNullOrEmpty(statecode))
                throw new ArgumentException("State name and state code cannot be null or empty string");

            this.CountryId = countryid;
            this.StateName = statename;
            this.StateCode = statecode;
            this.StateServiceLocations = new HashSet<StateServiceLocation>();
            this.Counties = new HashSet<County>();
        }

        public int Id { get; set; }
        public int CountryId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public bool Active { get; set; }
        public bool UserCreated { get; set; }

        public Country Country { get; set; }
        public ICollection<StateServiceLocation> StateServiceLocations { get; set; }
        public ICollection<County> Counties { get; set; }
    }
}
