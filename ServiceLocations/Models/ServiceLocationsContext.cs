﻿/*
*ServiceLocationsContext.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Configuration;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models
{
    public partial class ServiceLocationsContext : DbContext
    { 
        public virtual DbSet<CityServiceLocation> CityServiceLocations { get; set; }
        public virtual DbSet<CountyServiceLocation> CountyServiceLocations { get; set; }        
        public virtual DbSet<StateServiceLocation> StateServiceLocations { get; set; }
        public virtual DbSet<CountryServiceLocation> CountryServiceLocations { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<County> Counties { get; set; }        
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Country> Countries { get; set; }

        public ServiceLocationsContext(DbContextOptions options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CityServiceLocation>(entity =>
            {
                entity.HasIndex(e => e.Active)
                    .HasName("IX_CityServiceLocations_Active");

                entity.HasIndex(e => e.Id);

                entity.HasIndex(e => new { e.CountyServiceLocation, e.City })
                    .HasName("UK_City_CountyServiceLocation")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.Active })
                    .HasName("IX_CityServiceLocations_Id_Active"); ;

                entity.HasIndex(e => new { e.CityId, e.Active })
                    .HasName("IX_CityServiceLocations_CityId_Active"); ;

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CityId).HasColumnName("cityid");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CountyServiceId).HasColumnName("countyserviceid");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.HasOne(d => d.CountyServiceLocation)
                    .WithMany(p => p.CityServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CityServiceLocations_CountyServiceLocations");

                entity.HasOne(c => c.City)
                    .WithMany(p => p.CityServiceLocations)
                    .HasForeignKey(c => c.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CityServiceLocations_City");
            });

            modelBuilder.Entity<CountyServiceLocation>(entity =>
            {
                entity.HasIndex(e => e.Active)
                    .HasName("IX_CountyServiceLocations_Active");

                entity.HasIndex(e => new { e.StateServiceId, e.Active })
                    .HasName("IX_CountyServiceLocations_siteid_active");

                entity.HasIndex(e => new { e.StateServiceLocation, e.County })
                    .HasName("UK_County_StateServiceLocation")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CountyId).HasColumnName("countyid");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.StateServiceId).HasColumnName("stateserviceid");

                entity.Property(e => e.Tax).HasColumnType("decimal(5, 2)");

                entity.HasOne(d => d.County)
                    .WithMany(c => c.CountyServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CountyServiceLocations_County");

                entity.HasOne(d => d.StateServiceLocation)
                    .WithMany(p => p.CountyServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CountyServiceLocations_StateServiceLocations");
            });

            modelBuilder.Entity<StateServiceLocation>(entity =>
            {
                entity.HasIndex(e => e.Active)
                    .HasName("IX_StateServiceLocations_Active");

                entity.HasIndex(e => new { e.CountryServiceLocation, e.State })
                    .HasName("UK_State_CountryServiceLocation")
                    .IsUnique();


                entity.HasIndex(e => new { e.StateId, e.Active });

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.StateId).HasColumnName("stateid");
            
                entity.HasOne(d => d.State)
                    .WithMany(p => p.StateServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_StateServiceLocations_State");

                entity.HasOne(d => d.CountryServiceLocation)
                    .WithMany(d => d.StateServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CountryServiceLocation_StateServiceLocation");
            });

            modelBuilder.Entity<CountryServiceLocation>(entity =>
            {
                entity.HasIndex(e => e.Active)
                    .HasName("IX_CountryServiceLocations_Active");

                entity.HasIndex(e => new { e.CountryId, e.Active });

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.CountryId).HasColumnName("countryid");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CountryServiceLocations)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CountryServiceLocations_Country");

            });

        
            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("City");

                entity.HasIndex(e => e.CountyId);

                entity.HasIndex(e => new { e.CityName, e.CountyId })
                    .HasName("UK_Counties")
                    .IsUnique();

                entity.HasIndex(e => new { e.CountyId, e.CityName });

                entity.HasIndex(e => new { e.CountyId, e.UserCreated });

                entity.HasIndex(e => new { e.CountyId, e.CityName, e.UserCreated });

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CityName)
                    .IsRequired()
                    .HasColumnName("cityname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CountyId).HasColumnName("countyid");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserCreated)
                    .HasColumnName("usercreated")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.Cities)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_City_County");

                entity.HasMany(d => d.CityServiceLocations)
                    .WithOne(c => c.City)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_City_CityServiceLocation");
            });

            modelBuilder.Entity<County>(entity =>
            {
                entity.ToTable("County");

                entity.HasIndex(e => e.StateId);

                entity.HasIndex(e => new { e.CountyName, e.StateId })
                    .HasName("UK_CountyName_State")
                    .IsUnique();

                entity.HasIndex(e => new { e.StateId, e.UserCreated });

                entity.HasIndex(e => new { e.StateId, e.CountyName, e.UserCreated });

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CountyName)
                    .IsRequired()
                    .HasColumnName("countyname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StateId).HasColumnName("stateid");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserCreated)
                    .HasColumnName("usercreated")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Counties)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_Counties_State");

                entity.HasMany(d => d.CountyServiceLocations)
                    .WithOne(c => c.County)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_County_CountyServiceLocation");
            });           

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("State");

                entity.HasIndex(e => e.CountryId).HasName("IX_State_Country");

                entity.HasIndex(e => new { e.StateName, e.CountryId })
                    .HasName("UK_CountyName_State")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CountryId).HasColumnName("countryid");

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("statecode");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserCreated)
                    .HasColumnName("usercreated")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StateName).HasMaxLength(100);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.States)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_State_Country");

                entity.HasMany(d => d.StateServiceLocations)
                    .WithOne(c => c.State)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_State_StateServiceLocation");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("Country");

                entity.HasIndex(e => new { e.CountryName })
                    .HasName("UK_CountryName")
                    .IsUnique();

                entity.HasIndex(e => new { e.CountryCode })
                    .HasName("UK_CountryCode")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasColumnName("countryname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("countrycode");                

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserCreated)
                    .HasColumnName("usercreated")
                    .HasDefaultValueSql("((1))");

                entity.HasMany(d => d.CountryServiceLocations)
                    .WithOne(c => c.Country)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK_Country_CountryServiceLocation");

            });

        }
    }
}
