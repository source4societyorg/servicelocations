﻿/*
*ServiceLocationService.cs
*Part of the ServiceLocations Module for .NET Standard 2.0
*Copyright (C) 2017 Source4Society
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Source4Society.ComponentsDotNet.Modules.ServiceLocations.Factories;
using Source4Society.ComponentsDotNet.Modules.ServiceLocations.Models;

namespace Source4Society.ComponentsDotNet.Modules.ServiceLocations.Services
{
    /// <summary>
    /// This service provides functionality to associate active service locations with other data by country, state/province, county and city. It streamlines the adding, removing, and editing processes for these service locations
    /// </summary>
    class ServiceLocationsService
    {
        /// <summary>
        /// stores the db connection options specified in the constructor
        /// </summary>        
        protected DbContextOptions options;

        /// <summary>
        /// Factory class for constructing service location entities
        /// </summary>
        protected ServiceLocationFactory Factory;

        /// <summary>
        /// Constructor for the ServiceLocationService, initializes the context object
        /// </summary>
        /// <param name="options">DbContextOptions for configuring your database connection string</param>
        ServiceLocationsService(DbContextOptions options)
        {
            this.options = options;
            this.Factory = new ServiceLocationFactory(options);
        }

        /// <summary>
        /// Add a service location record. Will create each service record below country if it doesn't already exist. The country service record should be created by your implementation so it can be linked to a specific identifier for the stakeholder it belongs to (siteid, userid, etc.)
        /// </summary>
        /// <param name="CountryServiceLocation">The country service location record to associate this service location with</param>
        /// <param name="State">The state entity to associate this service location with</param>
        /// <param name="County">The county entity to associate this service location with</param>
        /// <param name="City">The city entity to associate this service location with</param>
        /// <param name="Tax">decimal? the tax associated with this service location (currently stored at the county level)</param>
        public void AddServiceLocation(CountryServiceLocation CountryServiceLocation, State State, County County, City City, decimal? tax)
        {
            StateServiceLocation StateServiceLocation = _StateServiceLocationHelper(State, CountryServiceLocation);
            CountyServiceLocation CountyServiceLocation = _CountyServiceLocationHelper(County, StateServiceLocation, tax);
            CityServiceLocation CityServiceLocation = _CityServiceLocationHelper(City, CountyServiceLocation);
            
            //TODO: Add countyservicelocation and cityservicelocation to the record.
        }

        /// <summary>
        /// A helper method that prepares the state service location based on whether it is present in the database, active, or inactive.
        /// </summary>
        /// <param name="State">State object to associate with StateServiceLocation</param>
        /// <param name="CountryServiceLocation">CountryServiceLocation object to associate with StateServiceLocation</param>
        /// <returns></returns>
        protected StateServiceLocation _StateServiceLocationHelper(State State, CountryServiceLocation CountryServiceLocation)
        {
            StateServiceLocation StateServiceLocation = CountryServiceLocation.StateServiceLocations.Where((StateServiceLocations) => { return StateServiceLocations.State == State; }).SingleOrDefault();
            if (StateServiceLocation == null)
            {
                StateServiceLocation = this.Factory.AddStateServiceLocation(State, CountryServiceLocation);
            }
            else
            {
                if (!StateServiceLocation.Active)
                {
                    StateServiceLocation.Active = true;
                    this.Factory.SaveStateServiceLocation(StateServiceLocation);
                }
            }

            return StateServiceLocation;
        }

        /// <summary>
        /// A helper method that prepares the county service location based on whether it is present in the database, active, or inactive. 
        /// </summary>
        /// <param name="County">County to associate with CountyServiceLocation</param>
        /// <param name="StateServiceLocation"> StateServiceLocation object to associate with CountyServiceLocation</param>
        /// <param name="tax">The decimal representation of the tax rate for the given county</param>
        /// <returns></returns>
        protected CountyServiceLocation _CountyServiceLocationHelper(County County, StateServiceLocation StateServiceLocation, decimal? tax)
        {
            bool updaterecord = false;
            CountyServiceLocation CountyServiceLocation = StateServiceLocation.CountyServiceLocations.Where((CountyServiceLocations) => { return CountyServiceLocations.County == County; }).SingleOrDefault();

            if (CountyServiceLocation == null)
            {
                CountyServiceLocation = this.Factory.AddCountyServiceLocation(County, StateServiceLocation, tax);
            }
            else
            {
                if (CountyServiceLocation.Tax != tax)
                {
                    CountyServiceLocation.Tax = tax;
                    updaterecord = true;
                }

                if (!CountyServiceLocation.Active == true)
                {
                    CountyServiceLocation.Active = true;
                    updaterecord = true;
                }

                if (updaterecord)
                {
                    this.Factory.SaveCountyServiceLocation(CountyServiceLocation);
                    updaterecord = false;
                }
            }

            return CountyServiceLocation;
        }

        /// <summary>
        /// A helper method that prepares the city service location based on whether it is present in the database, active, or inactive.
        /// </summary>
        /// <param name="City">City to associate with CityServiceLocation</param>
        /// <param name="CountyServiceLocation">CountyServiceLocation object to associate with CityServiceLocation</param>
        /// <returns></returns>
        protected CityServiceLocation _CityServiceLocationHelper(City City, CountyServiceLocation CountyServiceLocation)
        {
            CityServiceLocation CityServiceLocation = CountyServiceLocation.CityServiceLocations.Where((CityServiceLocations) => { return CityServiceLocations.City == City; }).SingleOrDefault();

            if (CityServiceLocation == null)
            {
                CityServiceLocation = this.Factory.AddCityServiceLocation(City, CountyServiceLocation);
            }
            else
            {
                if (!CityServiceLocation.Active == true)
                {
                    CityServiceLocation.Active = true;
                    this.Factory.SaveCityServiceLocation(CityServiceLocation);
                }
            }

            return CityServiceLocation;
        }
    }
}
