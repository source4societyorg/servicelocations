
Service Location Module
======================

This is a .NET Standard 2.0 module for implementing support for service locations based on country/state/county/city. It can be used for non US provinces as well. The data stored by the API is intended to be linked to data within your application that partitions each service location by some entity such as a site id, user id, or similar structure.

## Table of contents

[TOC]

## Installation

### NuGet

    To Do

### Generate Database Tables

    To Do

### Run Unit Tests

    To Do

## Usage

### Creating the Service

    To Do

### Loading location entities

    To Do

### Adding / Updating Service Locations

    To Do

## License

The Service Location Module is licensed under the terms of the GPL version 3 Open Source license and is available for free. [GPLv3 License Text](https://bitbucket.org/source4societyorg/servicelocations/src/6316138570ccac1c0faae4a9fc83359fcf443e5c/License.txt)

## Links

* [Source4Society.org](https://source4society.org) (Under construction)

## Contributions

All contributions welcome. Just put in a pull request for the development branch and we will review it.
